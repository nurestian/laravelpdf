@extends('welcome')

@section('content')
<div class="d-flex">
      <div class="p-2"><h2>Surat Jalan</h2> </div>
      <div class="ml-auto p-2">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal1"><i class="fa fa-plus"></i> Tambah Baru</button>
      </div>
    </div>
<div class="table-responsive">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Surat Jalan</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 0; ?>
         @foreach($suratjalan as $sj)
         <?php $no++; ?>
      <tr>
        <td>{{$no}}</td>
        <td>{{$sj->kode_surat_jalan}}</td>
        <td>
          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-view{{$sj->id}}">
                      <i class="iconfont fa fa-eye"></i>
                  </button>
              <a data-toggle="modal" data-target="#" href="#">
                  <button class="btn btn-danger btn-sm">
                  <i class="fa fa-trash" aria-hidden="true"></i>
                  </button>
              </a>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
  <!-- TAMBAH BARU -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <!-- Change class .modal-sm to change the size of the modal -->
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Tambah Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('surat_jalan.store')}}" method="post" enctype="multipart/form-data" >
                @csrf
            <div class="modal-body mx-3">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="inputAddress">Kode Surat Jalan</label>
                    <input type="text" class="form-control" id="nama" name="kode_surat_jalan">
                  </div>
                </div>
              </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success btn-sm">Save changes</button>
          </div>
          </form>
        </div>
      </div>
    </div>
    <!-- TAMBAH BARU  -->

</div>
@endsection
