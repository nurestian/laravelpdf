<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suratJalanDetail extends Model
{
  protected $fillable = [
  'id_surat_jalan',
  'nama_barang',
  'harga_beli',
  'stok'
];

public function suratJalan()
{
  return $this->belongsTo(\App\suratJalan::class, 'id_surat_jalan');
}

}
