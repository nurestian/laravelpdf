<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suratJalan extends Model
{
  protected $fillable = [
  'kode_surat_jalan'
];

public function suratJalanDetail()
{
  return $this->hasMany(\App\suratJalanDetail::class, 'id_surat_jalan');
}

}
