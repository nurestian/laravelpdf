<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\suratJalan;
use App\suratJalanDetail;
class SuratJalanDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $suratjalan = suratJalan::all();
      $suratjalandetail = suratJalandetail::all();
      return view('suratJalanDetail',compact('suratjalan', 'suratjalandetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $suratjalandetail = new suratJalanDetail;
      $suratjalandetail->id_surat_jalan = $request->get('kode_surat_jalan');
      $suratjalandetail->nama_barang = $request->get('nama');
      $suratjalandetail->harga_beli = $request->get('harga_beli');
      $suratjalandetail->stok = $request->get('stok');
      $suratjalandetail->save();

      return redirect('surat_jalan_detail');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
