<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratJalanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_jalan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_surat_jalan')->unsigned();
            $table->foreign('id_surat_jalan')->references('id')->on('surat_jalans')->onDelete('CASCADE');
            $table->string('nama_barang');
            $table->integer('harga_beli');
            $table->integer('stok');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_jalan_details');
    }
}
